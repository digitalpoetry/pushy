<?php

/*
 * Ajax Controller for Subscriptions
 */

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use
	DataTables\Database,
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// Database Connection
$db = new Database( $sql_details );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'subscriptions' )
	->fields(
		Field::inst( 'subscriptions.ID' )
			->set(false), // ID is automatically set by the database on create
		Field::inst( 'segments.segment' ),
		Field::inst( 'subscriptions.subscribed_on' )
			->validator( Validate::dateFormat(
				'm-d-Y g:i A',
				ValidateOptions::inst()
					->allowEmpty( false )
			) )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) )
			->setFormatter( Format::datetime(
				'm-d-Y g:i A',
				'Y-m-d H:i:s'
			) ),
		Field::inst( 'subscriptions.pushed_on' )
			->validator( Validate::dateFormat(
				'm-d-Y g:i A',
				ValidateOptions::inst()
					->allowEmpty( true )
			) )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) )
			->setFormatter( Format::datetime(
				'm-d-Y g:i A',
				'Y-m-d H:i:s'
			) ),
		Field::inst( 'subscriptions.push_count' )
			->validator( Validate::numeric() )
			->setFormatter( Format::ifEmpty(0) ),
		Field::inst( 'subscriptions.push_status' ),
		Field::inst( 'subscriptions.endpoint' ),
		Field::inst( 'subscriptions.expiration_time' )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) ),
		Field::inst( 'subscriptions.key_auth' ),
		Field::inst( 'subscriptions.key_p256dh' )/*,
		Field::inst( 'subscriptions.segment_id' )
			// ->get(false)
			->options( Options::inst()
				->table( 'segments' )
				->value( 'segments.ID' )
				->label( 'segments.segment' )
			)*/
	)
	->leftJoin( 'segments', 'subscriptions.segment_id', '=', 'segments.ID' )
	->process( $_POST )
	->json();
