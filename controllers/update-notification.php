<?php
/*
 * Ajax Controller for Subscriptions
 *
 * @See https://codepen.io/dericksozo/post/fetch-api-json-php
 */

// Allow CORS, set headers
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: *");

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use DataTables\Database;

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
	
	// Get the RAW post data.
	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	// Check if the json decoded
	if ( is_array($decoded) ) {

		// Set vars to save
		$action				= $decoded['action'];
		$subscription_id	= $decoded['data']['subscription_id'];
		$sent_id			= $decoded['data']['sent_id'];

		// Connect to the databaseb
		$db = new Database( $sql_details );

		// Update in database
		$db->update(
			'subscriptions',
			[
				'push_status' => $action
			],
			[
				'ID' => $subscription_id
			]
		);
		$db->update(
			'sent_notifications',
			[
				'status' => $action, 
				'interacted_on' => date("Y-m-d H:i:s"),
			],
			[
				'ID' => $sent_id
			]
		);

	}
}
