<?php
/*
 * Ajax Controller for Subscriptions
 *
 * @See https://codepen.io/dericksozo/post/fetch-api-json-php
 */

// Allow CORS, set headers
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: *");

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use DataTables\Database;

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
	
	// Get the RAW post data.
	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	// Check if the json decoded
	if ( is_array($decoded) ) {

		// print_r($decoded);

		// Set vars to save
		$segment			= isset($_GET['segment']) ? $_GET['segment']: '';
		$endpoint			= $decoded['endpoint'];
		$expiration_time	= $decoded['expirationTime'];
		$keys_auth			= $decoded['keys']['auth'];
		$keys_p256dh		= $decoded['keys']['p256dh'];

		// Check for an endpoint
		if ( !empty($endpoint) ) {

			// Connect to the databaseb
			$db = new Database( $sql_details );

			// 
			// $segment = $db->select( 'segments', '*', ['segment' => $segment] )->count();
			// $segment = $db->select( 'segments', '*', ['segment' => $segment] )->fetch();

			// Insert Subscription
			$db->insert(
				'subscriptions',
				[
					'segment'			=> $segment,
					'endpoint'			=> $endpoint,
					'expiration_time'	=> $expiration_time,
					'key_auth'			=> $keys_auth,
					'key_p256dh'		=> $keys_p256dh,
					'subscribed_on'		=> date("Y-m-d H:i:s")
				],
				'ID'
			);
			// $sentID = $result->insertId();


		}
	}
}
