<?php
/*
 * Ajax Controller for Subscriptions
 *
 * @See https://codepen.io/dericksozo/post/fetch-api-json-php
 */

// Allow CORS, set headers
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: *");

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use DataTables\Database;

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
	
	// Get the RAW post data.
	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	// Check if the json decoded
	if ( is_array($decoded) ) {

		// Set vars
		$keys_p256dh = $decoded['keys']['p256dh'];

		// Check for an endpoint key
		if ( !empty($keys_p256dh) ) {

			// Connect to the databaseb
			$db = new Database( $sql_details );

			// Delete Subscription
			$db->delete (
				'subscriptions',
				[
					'key_p256dh' => $keys_p256dh
				]
			);
		}
	}
}
