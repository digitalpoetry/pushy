<?php

/*
 * Ajax Controller for Notifications
 */

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use
	DataTables\Database,
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions,
	Minishlink\WebPush\WebPush,
	Minishlink\WebPush\Subscription;

// Database Connection
$db = new Database( $sql_details );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'notifications' )
	->fields(
		Field::inst( 'segment' )
			->get(false)
			->set(false)
			->validator( Validate::required() )
	)
	->process( $_POST )
	->json();

/*
 * Push notifications to the submitted subscriber segments
 */

// Get Subscriptions
$subscriptions = $db->select( 'subscriptions', '*', function ( $q ) {
	$firstRow = array_key_first($_POST['data']);
	foreach ( $_POST['data'][$firstRow]['segment'] as $segmentID ) {
		$q->or_where( 'segment_id', $segmentID, '=' );
	}
} )->fetchAll();

// Get Notifications
$notifications = $db->select( 'notifications', '*', function ( $q ) {
	foreach ( $_POST['data'] as $rowID => $data ) {
		$notificationID = substr($rowID, 4);
		$q->or_where( 'ID', $notificationID, '=' );
	}
} )->fetchAll();

// Set host url
$host = $_SERVER['HTTP_ORIGIN'];

// Loop thru each notification
$push = [];
foreach ( $notifications as $notification ) {

	// Get file paths
	$image			= $db->select( 'files_images', 'web_path', ['ID' => $notification['image']] )->fetch();
	$icon			= $db->select( 'files_icons', 'web_path', ['ID' => $notification['icon']] )->fetch();
	$badge			= $db->select( 'files_badges', 'web_path', ['ID' => $notification['badge']] )->fetch();
	$action_1_icon	= $db->select( 'files_action_1_icons', 'web_path', ['ID' => $notification['action_1_icon']] )->fetch();
	$action_2_icon	= $db->select( 'files_action_2_icons', 'web_path', ['ID' => $notification['action_2_icon']] )->fetch();
	$sound			= $db->select( 'files_sounds', 'web_path', ['ID' => $notification['sound']] )->fetch();

	//	Loop thru each subscription
	foreach ( $subscriptions as $subscription ) {

		// Log to database
		$result = $db->insert( 
			'sent_notifications',
			[
				'segment_id' 		=> $subscription['segment_id'],
				'subscription_id' 	=> $subscription['ID'],
				'notification_id' 	=> $notification['ID'],
				'pushed_on' 		=> date("Y-m-d H:i:s")
			],
			'ID'
		);
		$sentID = $result->insertId();
		// print_r($sentID);

		// Build the subscription
		$sub = Subscription::create([
			'endpoint' => $subscription['endpoint'],
			'keys' => [
				'p256dh' => $subscription['key_p256dh'],
				'auth'   => $subscription['key_auth']
			],
		]);

		// Build the payload, IGNORE ERRORS
		@$payload = '{
			"tag": "' . $notification['tag'] . '",
			"image": "' . $host . $image['web_path'] . '",
			"icon": "' . $host . $icon['web_path'] . '",
			"badge": "' . $host . $badge['web_path'] . '",
			"title": "' . $notification['title'] . '",
			"body": "' . $notification['body'] . '",
			"actions": [
				{
					"action": "action_1",
					"icon": "' . $host . $action_1_icon['web_path'] . '",
					"title": "' . $notification['action_1_title'] . '"
				},
				{
					"action": "action_2",
					"icon": "' . $host . $action_2_icon['web_path'] . '",
					"title": "' . $notification['action_2_title'] . '"
				}
			],
			"requireInteraction": ' . $notification['require_interaction'] . ',
			"renotify": ' . $notification['renotify'] . ',
			"silent": ' . $notification['silent'] . ',
			"sound": "' . $host . $sound['web_path'] . '",
			"vibrate": [' . $notification['vibrate'] . '],
			"timestamp": "' . strtotime($notification['timestamp']) . '",
			"data": {
				"subscription_id": "' . $subscription['ID'] . '",
				"notification_id": "' . $notification['ID'] . '",
				"sent_id": "' . $sentID . '",
				"default_url": "' . $notification['default_url'] . '",
				"action_2": "' . $notification['action_2'] . '",
				"action_1_url": "' . $notification['action_1_url'] . '",
				"action_2_url": "' . $notification['action_2_url'] . '"
			}
		}';

		// Add to the push array
		$push[] = [
			'segment_id'		=> $subscription['segment_id'],
			'subscription_id'	=> $subscription['ID'],
			'notification_id'	=> $notification['ID'],
			'push_count'		=> $subscription['push_count'],
			'sent_id'			=> $sentID,
			'subscription' 		=> $sub,
			'payload'			=> $payload
		];
	}
}

// Set push VAPID authorization
$auth = [
	'VAPID' => [
		'subject' => 'easysearchhelper.com', // can be a mailto: or your website address
		'publicKey' => 'BGnGkmUNUV684GvponZ2XjpcttGRMX7XnlryRjK8g1DC4iTyTYbgR0UxaNpDahFmMOwDZAaasuaGLaT6dm45Ri4=', // Uncompressed public key P-256 encoded in Base64-URL
		'privateKey' => 'AOLV2r0nQqZjuUJgay7NriAneeynnCQS6-9COMZ2Uxo=', // Private key encoded in Base64-URL
	],
];

// Create a push object
$webPush = new WebPush($auth);
$webPush->setReuseVAPIDHeaders(true);
$webPush->setAutomaticPadding(true);

// Iterate notifications
$once = false;
foreach ($push as $push_notification) {
	
	// Push the notification
	$report = $webPush->sendNotification(
		$push_notification['subscription'],
		$push_notification['payload'],
		true // flush report
	);

	// Get report
	foreach ($report as $report) {
		if ($report->isSuccess()) {
			$status = 'Sent';
		} else {
			// Log push report to database
			// $endpoint = $report->getEndpoint();
			// $failReason = $report->getReason();
			// $isTheEndpointWrongOrExpired = $report->isSubscriptionExpired();
			// $requestToPushService = $report->getRequest();
			// $responseOfPushService = $report->getResponse();
			$status = $report->isSubscriptionExpired() ? "Subscription Expired" : $report->getReason();
		}
	}

	// Log to database
	if ( !$once ) {
		$db->update(
			'segments',
			[
				'pushed_on' => date("Y-m-d H:i:s"),
			],
			[
				'ID' => $push_notification['segment_id']
			]
		);
		$once = true;
	}

	$db->update(
		'sent_notifications',
		[
			'status' => $status
		],
		[
			'ID' => $push_notification['sent_id']
		]
	);

	// Is subscription expired?
	if ( $report->isSubscriptionExpired() ) {

		// Delete subscription
		$db->delete(
			'subscriptions',
			[
				'ID' => $push_notification['subscription_id']
			]
		);

	} else {

		// Update subscription
		$db->update(
			'subscriptions',
			[
				'pushed_on'		=> date("Y-m-d H:i:s"),
				'push_count'	=> $push_notification['push_count']+1,
				'push_status'	=> $status,
			],
			[
				'ID' => $push_notification['subscription_id']
			]
		);
	}

}
