<?php

/*
 * Ajax Controller for Notifications
 */

// Alias Editor classes so they are easy to use
use
	DataTables\Database,
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Database Connection
$db = new Database( $sql_details );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'notifications' )
	->fields(
		Field::inst( 'ID' )
			->set(false), // ID is automatically set by the database on create
		Field::inst( 'tag' ),
		Field::inst( 'image' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/images/__ID__.__EXTN__' )
				->db( 'files_images', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 2000000, 'Files must be smaller that 2 Mb.' ) )
				->validator( Validate::fileExtensions( array( 'png', 'jpg', 'jpeg', 'gif' ), "Please upload a jpeg or png image." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'icon' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/icons/__ID__.__EXTN__' )
				->db( 'files_icons', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 500000, 'Files must be smaller that 500 Kb.' ) )
				->validator( Validate::fileExtensions( array( 'png', 'jpg', 'jpeg', 'gif' ), "Please upload a jpeg or png image." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'badge' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/badges/__ID__.__EXTN__' )
				->db( 'files_badges', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 500000, 'Files must be smaller that 500 Kb.' ) )
				->validator( Validate::fileExtensions( array( 'png', 'jpg', 'jpeg', 'gif' ), "Please upload a jpeg or png image." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'title' ),
		Field::inst( 'body' )
			->validator( Validate::maxLen( 500, ValidateOptions::inst()
				->message( 'The Body field may have a maximum of 500 characters.' )
			) ),
		Field::inst( 'default_url' )
			->validator( Validate::url( ValidateOptions::inst()
				->message( 'Please enter a valid URL' )
			) ),
		Field::inst( 'action_1_icon' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/action_1_icons/__ID__.__EXTN__' )
				->db( 'files_action_1_icons', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 500000, 'Files must be smaller that 500 Kb.' ) )
				->validator( Validate::fileExtensions( array( 'png', 'jpg', 'jpeg', 'gif' ), "Please upload a jpeg or png image." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'action_1_title' ),
		Field::inst( 'action_1_url' )
			->validator( Validate::url( ValidateOptions::inst()
				->message( 'Please enter a valid URL' )
			) ),
		Field::inst( 'action_2' )
			->validator( Validate::boolean() ),
		Field::inst( 'action_2_icon' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/action_2_icons/__ID__.__EXTN__' )
				->db( 'files_action_2_icons', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 500000, 'Files must be smaller that 500 Kb.' ) )
				->validator( Validate::fileExtensions( array( 'png', 'jpg', 'jpeg', 'gif' ), "Please upload a jpeg or png image." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'action_2_title' ),
		Field::inst( 'action_2_url' )
			->validator( Validate::url( ValidateOptions::inst()
				->message( 'Please enter a valid URL' )
			) ),
		Field::inst( 'timestamp' )
			->validator( Validate::dateFormat(
				'm-d-Y g:i A',
				ValidateOptions::inst()
					->allowEmpty( true )
			) )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) )
			->setFormatter( Format::datetime(
				'm-d-Y g:i A',
				'Y-m-d H:i:s'
			) ),
		Field::inst( 'ttl' )
			->validator( Validate::minMaxNum( 0, 30 ) ),
		Field::inst( 'urgency' )
			->validator( Validate::values( array( 'high', 'normal', 'low', 'very-low' ) ) ),
		Field::inst( 'silent' )
			->validator( Validate::boolean() ),
		Field::inst( 'sound' )
			->upload( Upload::inst( dirname(__DIR__).'/uploads/sounds/__ID__.__EXTN__' )
				->db( 'files_sounds', 'ID', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						unlink( $data[$i]['system_path'] );
					}
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( 500000, 'Files must be smaller that 500 Kb.' ) )
				->validator( Validate::fileExtensions( array( 'mp3', 'wav', 'ogg' ), "Please upload a mp3, wav or ogg sound file." ) )
			)
			->setFormatter( Format::ifEmpty( null ) ),
		Field::inst( 'vibrate' ),
		Field::inst( 'renotify' )
			->validator( Validate::boolean() ),
		Field::inst( 'require_interaction' )
			->validator( Validate::boolean() ),
		Field::inst( 'segment' )
			->get(false)
			->options( Options::inst()
				->table( 'segments' )
				->value( 'segments.ID' )
				->label( 'segments.segment' )
			)
	)
	->process( $_POST )
	->json();
