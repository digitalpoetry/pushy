<?php

/*
 * Ajax Controller for Sent Notifications
 */

// Include libraries & configuration
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/datatables.net/editor-php/config.php';

// Alias Editor classes so they are easy to use
use
	DataTables\Database,
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// Database Connection
$db = new Database( $sql_details );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'sent_notifications' )
	->fields(
		Field::inst( 'sent_notifications.ID' )
			->set(false), // ID is automatically set by the database on create
		Field::inst( 'segments.segment' ),
		Field::inst( 'notifications.tag' ),
		Field::inst( 'notifications.title' ),
		Field::inst( 'sent_notifications.pushed_on' )
			->validator( Validate::dateFormat(
				'm-d-Y g:i A',
				ValidateOptions::inst()
					->allowEmpty( false )
			) )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) )
			->setFormatter( Format::datetime(
				'm-d-Y g:i A',
				'Y-m-d H:i:s'
			) ),
		Field::inst( 'sent_notifications.interacted_on' )
			->validator( Validate::dateFormat(
				'm-d-Y g:i A',
				ValidateOptions::inst()
					->allowEmpty( true )
			) )
			->getFormatter( Format::datetime(
				'Y-m-d H:i:s',
				'm-d-Y g:i A'
			) )
			->setFormatter( Format::datetime(
				'm-d-Y g:i A',
				'Y-m-d H:i:s'
			) ),
		Field::inst( 'sent_notifications.status' )
	)
	->leftJoin( 'subscriptions', 'sent_notifications.subscription_id', '=', 'subscriptions.ID' )
	->leftJoin( 'notifications', 'sent_notifications.notification_id', '=', 'notifications.ID' )
	->leftJoin( 'segments', 'sent_notifications.segment_id', '=', 'segments.ID' )
	->process( $_POST )
	->json();
