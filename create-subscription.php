<?php

// Allow CORS, set headers
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Origin: *");

// @SEE https://codepen.io/dericksozo/post/fetch-api-json-php

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
	// Receive the RAW post data.
	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	// Check if the json decoded
	if ( is_array($decoded) ) {

		$endpoint			= $decoded['endpoint'];
		$expiration_time	= $decoded['expiration_time'];
		$keys_auth			= $decoded['keys']['auth'];
		$keys_p256dh		= $decoded['keys']['p256dh'];
		$segment			= isset($_GET['segment']) ? $_GET['segment']: '';

		// Check for an endpoint
		if ( !empty($endpoint) ) {
			// Connect to the database
			$db = mysqli_connect("localhost", "admin_pushy", "OI3qCNXRSH", "admin_pushy") // $host, $user, $pass, $db
			or die('Error connecting to MySQL server.');

			// Query for existing segment
			$query = "SELECT * FROM segments WHERE segment = '{$segment}';";
			$result = mysqli_query($db, $query);
			
			// Check for a result
			if ( mysqli_num_rows($result) == 0 ) {
				// Save segment to DB
				$query = "INSERT INTO `segments` (`segment`) VALUES ('{$segment}');";
				mysqli_query($db, $query);
				$segment_id = mysqli_insert_id($db);
			} else {
				// Get the segment ID
				$row = mysqli_fetch_array($result);
				$segment_id = $row['ID'];
			}
			mysqli_free_result($result);

			// Insert Subscription
			$query = "INSERT INTO `subscriptions` (`endpoint`, `expiration_time`, `key_auth`, `key_p256dh`, `segment_id`, `subscribed_on`) VALUES ('{$endpoint}', '{$expiration_time}', '{$keys_auth}', '{$keys_p256dh}', '{$segment_id}', NOW());";
			$result = mysqli_query($db, $query);
			mysqli_free_result($result);
			// echo json_encode(["status" => $result]);
		}
	}
}
