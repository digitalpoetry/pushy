--
-- Databse Structure for Notifications Manager
--

DROP TABLE IF EXISTS `subscriptions`;
DROP TABLE IF EXISTS `notifications`;
DROP TABLE IF EXISTS `sent_notifications`;
DROP TRIGGER IF EXISTS `subscriptions_insert`;
-- DROP TRIGGER IF EXISTS `subscriptions_update`;
DROP TRIGGER IF EXISTS `sent_notifications_insert`;
DROP TRIGGER IF EXISTS `sent_notifications_update`;

CREATE TABLE `segments` (
	`ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`segment` varchar(150) NOT NULL,
	`pushed_on` datetime DEFAULT NULL,
	`sub_count` int(5) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY ( ID ),
	UNIQUE ( segment )
) AUTO_INCREMENT=1;

CREATE TABLE `subscriptions` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`endpoint` varchar(255) NOT NULL,
	`expiration_time` timestamp NULL DEFAULT NULL,
	`key_auth` varchar(50) NOT NULL,
	`key_p256dh` varchar(150) NOT NULL,
	`segment_id` int(10) UNSIGNED NOT NULL,
	`subscribed_on` datetime NOT NULL,
	`pushed_on` datetime DEFAULT NULL,
	`push_count` int(5) UNSIGNED NOT NULL DEFAULT '0',
	`push_status` varchar(20) NOT NULL DEFAULT '',
	PRIMARY KEY ( ID ),
	UNIQUE KEY ( key_auth, key_p256dh )
) AUTO_INCREMENT=1;

CREATE TRIGGER subscriptions_insert BEFORE INSERT ON `subscriptions`
	FOR EACH ROW SET NEW.subscribed_on = IFNULL(NEW.subscribed_on, NOW());

-- CREATE TRIGGER subscriptions_update BEFORE UPDATE ON `subscriptions`
	-- FOR EACH ROW SET NEW.pushed_on = IFNULL(NEW.pushed_on, NOW());

CREATE TABLE `notifications` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`tag` varchar(100) DEFAULT '',
	`image` varchar(255) DEFAULT NULL,
	`icon` varchar(255) DEFAULT NULL,
	`badge` varchar(255) DEFAULT NULL,
	`title` varchar(255) DEFAULT NULL,
	`body` varchar(255) DEFAULT NULL,
	`default_url` varchar(255) DEFAULT NULL,
	`action_1_icon` varchar(255) DEFAULT NULL,
	`action_1_title` varchar(255) DEFAULT NULL,
	`action_1_url` varchar(255) DEFAULT NULL,
	`action_2` tinyint(1) UNSIGNED DEFAULT '0',
	`action_2_icon` varchar(255) DEFAULT NULL,
	`action_2_title` varchar(255) DEFAULT NULL,
	`action_2_url` varchar(255) DEFAULT NULL,
	`timestamp` datetime DEFAULT NULL,
	`ttl` varchar(50) DEFAULT NULL,
	`urgency` varchar(50) DEFAULT NULL,
	`silent` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
	`vibrate` varchar(50) DEFAULT NULL,
	`sound` varchar(255) DEFAULT NULL,
	`renotify` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
	`require_interaction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `sent_notifications` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`segment_id` int(10) UNSIGNED NOT NULL,
	`subscription_id` bigint(20) UNSIGNED NOT NULL,
	`notification_id` bigint(20) UNSIGNED NOT NULL,
	`pushed_on` datetime NOT NULL,
	`interacted_on` datetime DEFAULT NULL,
	`status` varchar(50) DEFAULT '',
	PRIMARY KEY ( ID )
	-- FOREIGN KEY (subscription_id) REFERENCES subscriptions(ID) ON DELETE CASCADE
	-- FOREIGN KEY (notification_id) REFERENCES notifications(ID) ON DELETE CASCADE
) AUTO_INCREMENT=1;

CREATE TRIGGER sent_notifications_insert BEFORE INSERT ON `sent_notifications`
	FOR EACH ROW SET NEW.pushed_on = IFNULL(NEW.pushed_on, NOW());

CREATE TRIGGER sent_notifications_update BEFORE UPDATE ON `sent_notifications`
	FOR EACH ROW SET NEW.interacted_on = IFNULL(NEW.interacted_on, NOW());

CREATE TABLE `files_action_1_icons` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `files_action_2_icons` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `files_badges` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `files_icons` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `files_images` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;

CREATE TABLE `files_sounds` (
	`ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`filename` varchar(100) DEFAULT NULL,
	`filesize` varchar(100) DEFAULT NULL,
	`web_path` varchar(255) DEFAULT NULL,
	`system_path` varchar(255) DEFAULT NULL,
	PRIMARY KEY ( ID )
) AUTO_INCREMENT=1;


/*
<?php


// The following statement can be removed after the first run (i.e. the database
// table has been created). It is a good idea to do this to help improve
// performance.
$db->sql( "CREATE TABLE IF NOT EXISTS `sdfsdf` (
	`id` int(10) NOT NULL auto_increment,
	`number` numeric(9,2),
	`sdfsdf` varchar(255),
	`sdfsdfdsfsdf` varchar(255),
	PRIMARY KEY( `id` )
);" );