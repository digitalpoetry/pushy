<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="https://www.datatables.net/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Subscriptions - Pushy</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
	<link rel="stylesheet" type="text/css" href="css/editor.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="css/notifications.manager.css">
	<!-- JS -->
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/dataTables.editor.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/editor.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/editor.chosen.js"></script>
	<script type="text/javascript" language="javascript" src="js/moment.js"></script>
	<script type="text/javascript" language="javascript" class="init">

		var editor; // use a global for the submit and return data rendering in the examples

		$(document).ready(function() {

			editor = new $.fn.dataTable.Editor({
				"ajax": "controllers/subscriptions.php",
				"table": "#subscriptions",
				"fields": [
					{
						label: "Subscriber Segment:",
						name: "subscriptions.segment_id",
						type:  "chosen",
						attr: {
							required: "required"
						}
					}
				]
			});

			$('#subscriptions').DataTable( {
				dom: "Bfrtip",
				ajax: {
					url: "controllers/subscriptions.php",
					type: "POST"
				},
				serverSide: true,
				columns: [
					{ data: "segments.segment" },
					{ data: "subscriptions.subscribed_on" },
					{ data: "subscriptions.pushed_on" },
					{ data: "subscriptions.push_count" },
					{ data: "subscriptions.push_status" },
					{ data: null, render: function ( data, type, row ) {
						return data.subscriptions.endpoint.substring(0, 35)+'...<br><strong>Expires on:</strong> '+data.subscriptions.expiration_time;
					}, "searchable": false },
					{ data: null, render: function ( data, type, row ) {
						return '<strong>Auth:</strong> '+data.subscriptions.key_auth+'<br><strong>p256dh:</strong> '+data.subscriptions.key_p256dh.substring(0, 35)+'...';
					}, "searchable": false }
				],
				order: [1, 'desc'],
				select: true,
				buttons: [
					{ extend: "edit",   editor: editor },
					{ extend: "remove", editor: editor },
					/*{
						extend: 'selected',
						text: 'Count selected rows',
						action: function ( e, dt, button, config ) {
							alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
						}
					}*/
				]
			});

		});
	</script>
</head>
<body>
	<?php include "menu.php"; ?>
	<?php include "authorization.php"; ?>
		<h1>Subscriptions</h1>
		<div class="table-responsive">
			<table id="subscriptions" class="table table-bordered table-hove table-sm" style="width:100%">
				<thead class="thead-light">
					<tr>
						<th scope="col">Segment</th>
						<th scope="col">Subscribed On</th>
						<th scope="col">Pushed On</th>
						<th scope="col">Push Count</th>
 						<th scope="col">Push Status</th>
						<th scope="col">Endpoint</th>
						<th scope="col">Keys</th> 
					</tr>
				</thead>
				<tfoot class="thead-light">
					<tr>
						<th scope="col">Segment</th>
						<th scope="col">Subscribed On</th>
						<th scope="col">Pushed On</th>
						<th scope="col">Push Count</th>
 						<th scope="col">Push Status</th>
						<th scope="col">Endpoint</th>
						<th scope="col">Keys</th> 
					</tr>
				</tfoot>
			</table>
		</div>
	<?php include "footer.php"; ?>
</body>
</html>