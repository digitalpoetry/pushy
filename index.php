<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="https://www.datatables.net/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Notifications - Pushy</title>
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/notifications.manager.css">
	
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/moment.js"></script>
</head>
<body>
	<?php include "menu.php"; ?>
	<?php include "authorization.php"; ?>
		<h1>Notifications Manager</h1>
		<p class="lead">Get started by creating a <a href="/notifications-manager/notifications.php">notification</a>, managing your <a href="/notifications-manager/subscriptions.php">subscribers</a>, or reviewing your <a href="/notifications-manager/sent-notifications.php">sent notification</a> statuses.</p>
	<?php include "footer.php"; ?>
</body>
</html>