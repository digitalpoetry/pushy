<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="https://www.datatables.net/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Segments - Pushy</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="css/editor.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="css/notifications.manager.css">
	<!-- JS -->
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/dataTables.editor.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/editor.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/moment.js"></script>
	<script type="text/javascript" language="javascript" class="init">

		var editor;

		$(document).ready(function() {

			editor = new $.fn.dataTable.Editor({
				"ajax": "controllers/segments.php",
				"table": "#segments",
				"fields": [
					{
						"label": "Segment Name:",
						"name": "segment"
					}
				]
			});

			$('#segments').DataTable( {
				dom: "Bfrtip",
				ajax: {
					url: "controllers/segments.php",
					type: "POST"
				},
				serverSide: true,
				columns: [
					{ data: "segment" },
					{ data: "pushed_on" }
				],
				select: true,
				buttons: [
					{ extend: "create", editor: editor },
					{ extend: "edit",   editor: editor },
					{ extend: "remove", editor: editor },
				]
			});

		});
	</script>
</head>
<body>
	<?php include "menu.php"; ?>
	<?php include "authorization.php"; ?>
		<h1>Segments</h1>
		<div class="table-responsive">
			<table id="segments" class="table table-bordered table-hove table-sm" style="width:100%">
				<thead class="thead-light">
					<tr>
						<th scope="col">Segment</th>
						<th scope="col">Pushed On</th>
					</tr>
				</thead>
				<tfoot class="thead-light">
					<tr>
						<th scope="col">Segment</th>
						<th scope="col">Pushed On</th>
					</tr>
				</tfoot>
			</table>
		</div>
	<?php include "footer.php"; ?>
</body>
</html>