/*
 * Autorun and subscribe user to notifications
 *
 * SEE ALSO: https://www.sitepoint.com/how-to-use-push-notifications-for-web-applications/
 * SEE ALSO: https://developers.google.com/web/fundamentals/push-notifications
 */

// Get query vars
const params = new URLSearchParams(window.location.search);

// Register a service Worker
if ('serviceWorker' in navigator) {
	if ('PushManager' in window) {
		navigator.serviceWorker.register('/serviceworker.js')
		.then(function(registration) {
			// State initializing
		})
		.catch(function(e) {
			// Error handling
			console.log(e);
		});
	}
}

// Create a subscription
navigator.serviceWorker.ready.then(function(registration) {
	registration.pushManager.subscribe({
		userVisibleOnly: true,
		applicationServerKey: base64ToUint8Array('BGnGkmUNUV684GvponZ2XjpcttGRMX7XnlryRjK8g1DC4iTyTYbgR0UxaNpDahFmMOwDZAaasuaGLaT6dm45Ri4')
	})
	.then(function(pushSubscription) {
		// Save the subscription
		sendSubscriptionToBackEnd(pushSubscription);
	})
	.catch(function(e) {
		// Error handling
		console.log(e);
	});
});

// ----------------------------------------------------------------------------

/*
 * Save subscription info
 */
function sendSubscriptionToBackEnd(subscription) {
	if ( params.has('segment') )
	{
		var segment = params.get('segment');
	}
	else
	{
		// var segment = window.location.hostname;
		var segment = window.location.hostname + window.location.pathname;
	}
	return fetch('https://pushy.digital/notifications-manager/create-subscription.php?segment=' + segment, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(subscription)
	});
}

/*
 * Format the server public key
 */
function base64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}