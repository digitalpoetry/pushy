/*
 * Notifications Service Worker
 */

/*
 * Push event handler
 */

self.addEventListener('push', function(event) {

	// Get notification data
	var data = event.data.json();

	// Check if action 2 is enabled, remove if not
	if (data.data.action_2 == 0)
		data.actions.splice(1, 1);

	// Display a notification
	event.waitUntil(self.registration.showNotification(data.title, data));
});

/*
 * Click event handler
 */

self.addEventListener('notificationclick', function(event) {
	
	// Get notifiction data
	var data = event.notification.data;

	// Notification was clicked, but not an action
	if (!event.action) {
		// Open a webpage
		event.waitUntil(clients.openWindow(data.default_url));
		updateNotification('Clicked', data);
	}

	// Notification action was clicked
	switch (event.action) {
		case 'action_1':
			// Open a webpage
			event.waitUntil(clients.openWindow(data.action_1_url));
			// Log event to database
			updateNotification('Action 1 clicked', data);
			break;
		case 'action_2':
			// Open a webpage
			event.waitUntil(clients.openWindow(data.action_2_url));
			// Log event to database
			updateNotification('Action 2 clicked', data);
			break;
	}
	
	// Return promise
	return;
});

/*
 * Close event handler
 */

self.addEventListener('notificationclose', function(event) {
	
	// Get notifiction data
	var data = event.notification.data;

	// Log event to database
	updateNotification('Closed', data);
});

/*
 * Expiration / Renew event handler
 */

self.addEventListener('pushsubscriptionchange', function(registration, newSubscription, oldSubscription) {
	console.log('PHP: removeSubscription');
	// removeSubscription(oldSubscription);
	console.log('PHP: saveSubscription');
	// saveSubscription(newSubscription);
});

/*
 * Unsubscribe event handler
 */

/*registration.pushManager.getSubscription()
.then(function(subscription) {
	if (subscription) {
		return subscription.unsubscribe()
		.then(function(successful) {
			console.log('Removing Push Subscription...');
			// Delete the subscription
			// removeSubscription(subscription);
		}).catch(function(e) {
			// Silence errors
		});
	}
})
.catch(function(error) {
	// Silence errors
})*/

/*
 * Helpers
 */

// Update subscription in database
function updateNotification(action, data) {
	return fetch('https://pushy.digital/notifications-manager/controllers/update-notification.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			action: action,
			data: data
		})
	});
}

// Save subscription to database
function saveSubscription(subscription) {
	return fetch('https://pushy.digital/notifications-manager/controllers/create-subscription.php?segment=FIXME', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(subscription)
	});
}

// Update subscription in database
/*function updateSubscription(subscription) {
	var data = '';
	return fetch('https://pushy.digital/notifications-manager/controllers/update-subscription.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(subscription)
	});
}*/

// Remove subscription from database
function removeSubscription(subscription) {
	return fetch('https://pushy.digital/notifications-manager/controllers/delete-subscription.php', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(subscription)
	});
}
