	<header>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" href="/notifications-manager/">Pushy</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/notifications-manager/segments.php">Segments</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/notifications-manager/subscriptions.php">Subscriptions</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/notifications-manager/notifications.php">Notifications</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/notifications-manager/sent-notifications.php">Sent</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<main role="main" class="container-fluid">