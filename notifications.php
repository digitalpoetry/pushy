<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="https://www.datatables.net/favicon.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, user-scalable=no">
	<title>Notifications - Pushy</title>
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.4/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css">
	<link rel="stylesheet" type="text/css" href="css/editor.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="css/notifications.manager.css">
	
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.4/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.4/js/responsive.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/dataTables.editor.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/editor.bootstrap4.min.js"></script>
	<script type="text/javascript" language="javascript" src="js/editor.chosen.js"></script>
	<script type="text/javascript" language="javascript" src="js/moment.js"></script>
	<script type="text/javascript" language="javascript" class="init">

		// var editor; // use a global for the submit and return data rendering in the examples

		$(document).ready(function() {

			var pusher = new $.fn.dataTable.Editor({
				ajax: "controllers/push-notifications.php",
				table: "#notifications",
				fields: [
					{
						label: "Subscriber Segment:",
						name: "segment",
						type:  "chosen",
						attr: {
							multiple: "multiple",
							required: "required"
						}
					}
				]
			});

			var editor = new $.fn.dataTable.Editor({
				ajax: "controllers/notifications.php",
				table: "#notifications",
				fields: [
					{
						label: "Tag:",
						name: "tag",
						fieldInfo: "Enter a description for this field!"
					},
					{
						label: "Image:",
						name: "image",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<img src="'+editor.file( 'files_images', file_id ).web_path+'"/>';
						}
					},
					{
						label: "Icon:",
						name: "icon",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<img src="'+editor.file( 'files_icons', file_id ).web_path+'"/>';
						}
					},
					{
						label: "Badge:",
						name: "badge",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<img src="'+editor.file( 'files_badges', file_id ).web_path+'"/>';
						}
					},
					{
						label: "Title:",
						name: "title"
					},
					{
						label: "Body:",
						name: "body",
						type:  "textarea"
					},
					{
						label: "Default URL:",
						name: "default_url"
					},
					{
						label: "Action 1 Icon:",
						name: "action_1_icon",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<img src="'+editor.file( 'files_action_1_icons', file_id ).web_path+'"/>';
						}
					},
					{
						label: "Action 1 Label:",
						name: "action_1_title"
					},
					{
						label: "Action 1 URL:",
						name: "action_1_url"
					},
					{
						label: "Enable Action #2:",
						name: "action_2",
						type:  "checkbox",
						options: [
							{ label: "", value: 1 }
						],
						unselectedValue: 0,
						def: 0,
						separator: ""
					},
					{
						label: "Action 2 Icon:",
						name: "action_2_icon",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<img src="'+editor.file( 'files_action_2_icons', file_id ).web_path+'"/>';
						}
					},
					{
						label: "Action 2 Label:",
						name: "action_2_title"
					},
					{
						label: "Action 2 URL:",
						name: "action_2_url"
					},
					{
						label: "Show advanced options",
						name: "advanced_options",
						type:  "checkbox",
						options: [
							{ label: "", value: 1 }
						],
						unselectedValue: 0,
						def: 0,
						separator: ""
					},
					{
						label: "Timestamp:",
						name: "timestamp",
						type: 'datetime',
						format: "MM-DD-YYYY h:mm A"
					},
					{
						label: "TTL (# of days):",
						name: "ttl",
						attr: {
							type: "number",
							min: 0,
							max: 30
						},
						fieldInfo: "Enter a description for this field!"
					},
					{
						label: "Urgency:",
						name: "urgency",
						type:  "select",
						def: "normal",
						options: [
							{ label: "High",		value: "high" },
							{ label: "Normal",		value: "normal" },
							{ label: "Low",			value: "low" },
							{ label: "Very low",	value: "very-low" }
						]
					},
					{
						label: "Silent:",
						name: "silent",
						type:  "checkbox",
						options: [
							{ label: "", value: 1 }
						],
						unselectedValue: 0,
						separator: ""
					},
					{
						label: "Sound",
						name: "sound",
						type: "upload",
						clearText: "Remove",
						display: function ( file_id ) {
							return '<a href="'+editor.file( 'files_sounds', file_id ).web_path+'" target="_blank">Play</a>';
						}
					},
					{
						label: "Vibrate:",
						name: "vibrate",
						def: "200, 100, 200, 100, 200, 100, 400"
					},
					{
						label: "Renotify:",
						name: "renotify",
						type:  "checkbox",
						options: [
							{ label: "", value: 1 }
						],
						unselectedValue: 0,
						def: 1,
						separator: ""
					},
					{
						label: "Require Interaction:",
						name: "require_interaction",
						type:  "checkbox",
						options: [
							{ label: "", value: 1 }
						],
						unselectedValue: 0,
						def: 1,
						separator: ""
					}
				]
			});

			var table = $('#notifications').DataTable({
				ajax: {
					url: "controllers/notifications.php",
					type: "POST"
				},
				columns: [
					{ data: null, defaultContent: '' },  // 0
					{ data: "ID" },  // 1
					{ data: "tag" },  // 2
					{ data: "image", render: function ( file_id ) {  // 3
						return file_id ? '<img class="noti-image" src="'+editor.file( 'files_images', file_id ).web_path+'"/>' : null;
					} },
					{ data: "icon", render: function ( file_id ) {  // 4
						return file_id ? '<img class="noti-icon" src="'+editor.file( 'files_icons', file_id ).web_path+'"/>' : null;
					} },
					{ data: "badge", render: function ( file_id ) {  // 5
						return file_id ? '<img class="noti-badge" src="'+editor.file( 'files_badges', file_id ).web_path+'"/>' : null;
					} },
					{ data: "title" },  // 6
					{ data: "body" },  // 7
					{ data: "default_url" },  // 8
					{ data: null, render: function ( data, type, row ) {  // 9
						var action_1_icon = data.action_1_icon ? '<img class="noti-action-icon" src="'+editor.file( 'files_action_1_icons', data.action_1_icon ).web_path+'"/> ' : '';
						return '<a href="'+data.action_1_url+'" onclick="return false;">'+action_1_icon+data.action_1_title+'</a>';
					} },
					{ data: "action_1_icon" },  // 10
					{ data: "action_1_title" },  // 11
					{ data: "action_1_url" },  // 12
					{ data: null, render: function ( data, type, row ) {  // 13
						var action_2_icon = data.action_2_icon ? '<img class="noti-action-icon" src="'+editor.file( 'files_action_2_icons', data.action_2_icon ).web_path+'"/> ' : '';
						return '<a href="'+data.action_2_url+'" onclick="return false;">'+action_2_icon+data.action_2_title+'</a>';
					} },
					{ data: "action_2_icon" },  // 14
					{ data: "action_2_title" },  // 15
					{ data: "action_2_url" },  // 16
					{ data: "timestamp" },  // 17
					{ data: "ttl", render: function ( value ) {  // 18
						return value ? value + ' days' : null;
					} },
					{ data: "urgency" },  // 19
					{ data: "silent", render: function ( value ) {  // 20
						return (value == 0) ? 'No' : 'Yes';
					} },
					{ data: "sound", render: function ( file_id ) {  // 21
						// return file_id ? '<a href="'+editor.file( 'files_sounds', file_id ).web_path+'" target="_blank">Play</a>' : null;
						return file_id ? '<audio controls><source src="'+editor.file( 'files_sounds', file_id ).web_path+'" /></audio>' : null;
					} },
					{ data: "vibrate" },  // 22
					{ data: "renotify", render: function ( value ) {  // 23
						return (value == 0) ? 'No' : 'Yes';
					} },
					{ data: "require_interaction", render: function ( value ) {  // 24
						return (value == 0) ? 'No' : 'Yes';
					} }
				],
				columnDefs: [
					{	// 1) Responsive column priority highest
						responsivePriority: 1,
						targets: [ 0, 2, 6, 9, 13 ]
					},
					{	// 2) Responsive column priority higher
						responsivePriority: 2,
						targets: [ 3 ]
					},
					{	// 3) Responsive column priority high
						responsivePriority: 3,
						targets: [  7, 8 ]
					},
					{	// Control column, for child rows
						className: 'control',
						orderable: false,
						searchable:false,
						targets: 0
					},
					{	// ID column
						orderable: false,
						searchable:false,
						visible: false,
						targets: 1
					},
					{	// Visible, non-searchable 'action' columns
						visible: false,
						searchable:false,
						targets: [10, 14]
					},
					{	// Hidden, searchable 'action' columns
						visible: false,
						targets: [11, 12, 15, 16]
					},
					{	// Columns with files
						orderable: false,
						searchable:false,
						targets: [ 3, 4, 5, 9, 13, 21 ]
					},
					{	// All Columns, MUST be last definition
						className: 'align-middle',
						targets: '_all'
					}
				],
				lengthChange: false,
				order: [],
				processing: true,
				responsive: {
					details: {
						type: 'column'
					}
				},
				stateSave: true,
				serverSide: true,
				select: true,
			});

			// Display the buttons
			new $.fn.dataTable.Buttons( table, [
				{ extend: "create", editor: editor },
				{ extend: "edit",   editor: editor },
				{ extend: "remove", editor: editor },
				{
					extend: "edit",
					text: 'Push',
					editor: pusher
				}
			] );

			// Prepend Buttons to table
			table.buttons().container().appendTo( $('.col-md-6:eq(0)', table.table().container() ) );

			// Disable row selection on first column
			table.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
				if ( $(originalEvent.target).index() === 0 ) {
					e.preventDefault();
				}
			} );

			// Show/Hide dependant fields - Action 2
			editor.dependent( 'action_2', function ( val, data, callback ) {
				return val === '1' ?
					{ show: [ 'action_2_icon', 'action_2_title', 'action_2_url' ] } :
					{ hide: [ 'action_2_icon', 'action_2_title', 'action_2_url' ] };
			} );

			// Show/Hide dependant fields - Silent
			editor.dependent( 'silent', function ( val, data, callback ) {
				return val === '0' && editor.field( 'advanced_options' ).val() === '1' ?
					{ show: [ 'sound', 'vibrate' ] } :
					{ hide: [ 'sound', 'vibrate' ] };
			} );

			// Show/Hide dependant fields - Advanced options
			editor.dependent( 'advanced_options', function ( val, data, callback ) {
				var fields = [ 'ttl', 'urgency', 'silent', 'renotify', 'require_interaction', 'timestamp' ];
				// Check if 'sound' & 'vibrate' should be shown as well
				if ( editor.field( 'silent' ).val() === '0' )
					fields.push( 'sound', 'vibrate' );
				return val === '1' ?
					{ show: fields } :
					{ hide: [ 'ttl', 'urgency', 'silent', 'sound', 'vibrate', 'renotify', 'require_interaction', 'timestamp' ] };
			} );

		});
	</script>
</head>
<body>
	<?php include "menu.php"; ?>
	<?php include "authorization.php"; ?>
		<h1>Notifications</h1>
		<table id="notifications" class="table table-bordered table-hover table-sm" style="width:100%">
			<thead class="thead-light">
				<tr>
					<th scope="col"></th>
					<th scope="col">ID</th>
					<th scope="col">Tag</th>
					<th scope="col">Image</th>
					<th scope="col">Icon</th>
					<th scope="col">Badge</th>
					<th scope="col">Title</th>
					<th scope="col">Body</th>
					<th scope="col">Default URL</th>
					<th scope="col">Action 1</th>
					<th scope="col">Action 1 Icon</th>
					<th scope="col">Action 1 Label</th>
					<th scope="col">Action 1 URL</th>
					<th scope="col">Action 2</th>
					<th scope="col">Action 2 Icon</th>
					<th scope="col">Action 2 Label</th>
					<th scope="col">Action 2 URL</th>
					<th scope="col">Timestamp</th>
					<th scope="col">TTL</th>
					<th scope="col">Urgency</th>
					<th scope="col">Silent</th>
					<th scope="col">Sound</th>
					<th scope="col">Vibrate</th>
					<th scope="col">Renotify</th>
					<th scope="col">Require Interaction</th>
				</tr>
			</thead>
			<tfoot class="thead-light">
				<tr>
					<th scope="col"></th>
					<th scope="col">ID</th>
					<th scope="col">Tag</th>
					<th scope="col">Image</th>
					<th scope="col">Icon</th>
					<th scope="col">Badge</th>
					<th scope="col">Title</th>
					<th scope="col">Body</th>
					<th scope="col">Default URL</th>
					<th scope="col">Action 1</th>
					<th scope="col">Action 1 Icon</th>
					<th scope="col">Action 1 Label</th>
					<th scope="col">Action 1 URL</th>
					<th scope="col">Action 2</th>
					<th scope="col">Action 2 Icon</th>
					<th scope="col">Action 2 Label</th>
					<th scope="col">Action 2 URL</th>
					<th scope="col">Timestamp</th>
					<th scope="col">TTL</th>
					<th scope="col">Urgency</th>
					<th scope="col">Silent</th>
					<th scope="col">Sound</th>
					<th scope="col">Vibrate</th>
					<th scope="col">Renotify</th>
					<th scope="col">Require Interaction</th>
				</tr>
			</tfoot>
		</table>
	<?php include "footer.php"; ?>
</body>
</html>